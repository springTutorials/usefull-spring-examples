package com.vk.usefullspringexamples.lookupexample;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.GenericApplicationContext;

@Configuration
@ComponentScan("com.vk.usefullspringexamples.lookupexample")
public class LookupConfig {

    public static void main(String... args) {
        GenericApplicationContext ctx = new AnnotationConfigApplicationContext(LookupConfig.class);
        LookupExample lookup1 = ctx.getBean("lookupExample", LookupExample.class);
        LookupExample lookup2 = ctx.getBean("lookupExample", LookupExample.class);
        Car car1 = lookup1.getCar();
        Car car2 = lookup2.getCar();

        System.out.println("car1: " + car1);
        System.out.println("car2: " + car2);
        System.out.println("is same instance: " + (car1 == car2));
    }
}
