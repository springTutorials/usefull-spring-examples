package com.vk.usefullspringexamples.lookupexample;

import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

@Component
public class LookupExample {

    @Lookup
    public Car getCar() {
        return null;
    }
}
