package com.vk.usefullspringexamples.lookupexample;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class Car {

    @Override
    public String toString() {
        return "this is prototype car object";
    }
}
